package project.adywelski.sii.bitcoinmarket.nationalBank.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.nationalBank.dto.CurrencyDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class NationalBankRecyclerViewAdapter(val context: Context, private val currencyDTOList: List<CurrencyDTO>) : RecyclerView.Adapter<NationalBankViewHolder>(){

    private val layoutInflater : LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): NationalBankViewHolder {
        val view : View = layoutInflater.inflate(R.layout.national_bank_view_item, parent, false)

        return NationalBankViewHolder(view)
    }

    override fun onBindViewHolder(holder: NationalBankViewHolder?, position: Int) {
        val currencyDTO : CurrencyDTO = currencyDTOList[position]

        holder!!.currency.text = currencyDTO.currency
        holder.code.text = currencyDTO.code
        holder.ratesBid.text = currencyDTO.bid
        holder.ratesAsk.text = currencyDTO.ask

        holder.bind(currencyDTO)
    }

    override fun getItemCount(): Int {
        return currencyDTOList.size
    }


}