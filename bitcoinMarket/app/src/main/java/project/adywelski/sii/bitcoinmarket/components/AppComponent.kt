package project.adywelski.sii.bitcoinmarket.components

import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import project.adywelski.sii.bitcoinmarket.MainActivity
import project.adywelski.sii.bitcoinmarket.coinMarket.CoinMarketActivity
import project.adywelski.sii.bitcoinmarket.nationalBank.NationalBankActivity
import project.adywelski.sii.bitcoinmarket.selfImpl.CreateSelfActivity
import project.adywelski.sii.bitcoinmarket.selfImpl.SelfActivity

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
interface AppComponent {
    fun inject(application: FinanceMarketApplication)
    fun inject(activity: MainActivity)
    fun inject(activity: CoinMarketActivity)
    fun inject(activity: NationalBankActivity)
    fun inject(activity: SelfActivity)
    fun inject(activity: CreateSelfActivity)
}