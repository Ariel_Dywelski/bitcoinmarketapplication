package project.adywelski.sii.bitcoinmarket.nationalBank.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.nationalBank.dto.CurrencyDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class NationalBankViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var currency : TextView = itemView.findViewById(R.id.ratesNameTextView)
    var code : TextView = itemView.findViewById(R.id.ratesCodeTextView)
    var ratesBid : TextView = itemView.findViewById(R.id.ratesBuyPriceTextView)
    var ratesAsk : TextView = itemView.findViewById(R.id.ratesSellTextView)

    fun bind(currencyDTO: CurrencyDTO){
        currency.text = currencyDTO.currency
        code.text = currencyDTO.code
        ratesBid.text = currencyDTO.bid
        ratesAsk.text = currencyDTO.ask
    }
}