package project.adywelski.sii.bitcoinmarket.selfImpl.dto

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
data class SelfDTO(val rank: String,
                   val name: String,
                   val symbol: String,
                   val price_usd: String,
                   val price_pln: String)