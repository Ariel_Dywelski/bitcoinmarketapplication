package project.adywelski.sii.bitcoinmarket.selfImpl

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_self.*
import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.selfImpl.dto.SelfRealmDataDTO
import project.adywelski.sii.bitcoinmarket.selfImpl.view.SelfRecyclerViewAdapter

class SelfActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_self)

        ((application as FinanceMarketApplication)).getAppComponent().inject(this)

        val result = getResult()
        result.size

        recyclerView = findViewById(R.id.selfRealmRecyclerView)
        val selfRecyclerViewAdapter = SelfRecyclerViewAdapter(applicationContext, result)
        recyclerView.adapter = selfRecyclerViewAdapter
        recyclerView.layoutManager = LinearLayoutManager(applicationContext)


        new_fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).show()
            val createIntent = Intent(applicationContext, CreateSelfActivity::class.java)
            createIntent.putExtra("CreateExtras", "Create your custom card.")
            startActivity(createIntent)
        }
    }

    private fun getFromRealmDatabase() : RealmResults<SelfRealmDataDTO>{
        val realm = Realm.getDefaultInstance()
        return realm.where(SelfRealmDataDTO::class.java).distinct("coinSymbolString")
    }

    private fun getResult() : List<SelfRealmDataDTO>{
        return getFromRealmDatabase()
    }
}
