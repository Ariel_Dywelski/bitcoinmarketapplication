package project.adywelski.sii.bitcoinmarket.nationalBank

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_national_bank.*
import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.nationalBank.dto.CurrencyDTO
import project.adywelski.sii.bitcoinmarket.nationalBank.dto.NationalBankDTO
import project.adywelski.sii.bitcoinmarket.nationalBank.view.NationalBankRecyclerViewAdapter
import project.adywelski.sii.bitcoinmarket.services.NationalBankWebServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class NationalBankActivity : AppCompatActivity() {

    @Inject lateinit var nationalWebServices: NationalBankWebServices

    private var snackBarText: String = ""
    private lateinit var nationalLayout : View
    private lateinit var snackBar: Snackbar
    val NATIONAL_BANK_DTO : String = "http://api.nbp.pl/api/exchangerates/tables/c/"

    lateinit var recyclerView : RecyclerView
    private lateinit var swipeLayout : SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_national_bank)

        ((application as FinanceMarketApplication)).getAppComponent().inject(this)

        nationalLayout = findViewById(R.id.nationalBankLayout)
        recyclerView = findViewById(R.id.currencyRecyclerView)
        swipeLayout = findViewById(R.id.currencySwipeLayout)

        createSnackbar()
        currencyFloatingButton()

        swipeLayout.setOnRefreshListener { getCurrencyRatesData() }

        getCurrencyRatesData()
    }

    private fun getCurrencyRatesData() {
        nationalWebServices.getTopNationalCurrency(NATIONAL_BANK_DTO).enqueue(object : Callback<List<NationalBankDTO>> {
            override fun onResponse(call: Call<List<NationalBankDTO>>?, response: Response<List<NationalBankDTO>>?) {
                if (response != null) {
                    if (response.isSuccessful){
                        val body : List<NationalBankDTO> = response.body()!!
                        val rates : List<CurrencyDTO> = body[0].rates
                        if (body.isEmpty()) {
                            Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
                            return
                        } else {
                            Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()
                            val nationalBankRecyclerViewAdapter = NationalBankRecyclerViewAdapter(applicationContext, rates)
                            recyclerView.adapter = nationalBankRecyclerViewAdapter
                            recyclerView.layoutManager = LinearLayoutManager(applicationContext)

                            onItemLoadComplete()
                        }
                    }
                }
            }
            override fun onFailure(call: Call<List<NationalBankDTO>>?, t: Throwable?) {
                Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    private fun currencyFloatingButton() {
        new_fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }


    private fun createSnackbar() {
        val extras: String = intent.extras.get("extras").toString()
        if (extras == null) {
            snackBarText = "Null extras"
            snackBar = Snackbar.make(nationalLayout, snackBarText, Snackbar.LENGTH_LONG)
        } else {
            snackBarText = extras
            snackBar = Snackbar.make(nationalLayout, snackBarText, Snackbar.LENGTH_LONG)
            val view = snackBar.view
            val snackbarTextView: TextView = view.findViewById(android.support.design.R.id.snackbar_text)
            snackbarTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        snackBar.show()
    }
    private fun onItemLoadComplete(){
        swipeLayout.isRefreshing = false
    }
}
