package project.adywelski.sii.bitcoinmarket.components

import dagger.Component
import project.adywelski.sii.bitcoinmarket.module.AndroidModule
import project.adywelski.sii.bitcoinmarket.module.NetworkingModule
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
@Singleton
@Component(modules = arrayOf(AndroidModule::class, NetworkingModule::class))
interface AppStubComponent : AppComponent {
}