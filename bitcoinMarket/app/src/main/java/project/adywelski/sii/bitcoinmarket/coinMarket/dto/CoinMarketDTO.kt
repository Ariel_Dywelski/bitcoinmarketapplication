package project.adywelski.sii.bitcoinmarket.coinMarket.dto

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
data class CoinMarketDTO (val rank: String,
                          val name: String,
                          val symbol: String,
                          val price_usd: String,
                          val price_pln: String)