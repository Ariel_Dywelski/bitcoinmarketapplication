package project.adywelski.sii.bitcoinmarket.selfImpl

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_create_self.*
import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.coinMarket.dto.CoinMarketDTO
import project.adywelski.sii.bitcoinmarket.selfImpl.dto.SelfRealmDataDTO
import project.adywelski.sii.bitcoinmarket.services.CoinMarketWebServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject


class CreateSelfActivity : AppCompatActivity() {

    @Inject lateinit var coinWebServices: CoinMarketWebServices

    private var snackBarText: String = ""
    private lateinit var selfLayout: View
    private lateinit var snackBar: Snackbar
    private lateinit var spinner: Spinner

    var customList: ArrayList<String> = arrayListOf()
    var selfDTOList: List<CoinMarketDTO> = mutableListOf()

    val COIN_URL: String = "https://api.coinmarketcap.com/v1/ticker/?convert=PLN&limit=20"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_self)

        ((application as FinanceMarketApplication)).getAppComponent().inject(this)

        selfLayout = findViewById(R.id.selfLayout)
        spinner = findViewById(R.id.currencySpinner)

        createSnackbar()

        selfDTOList
        coinWebServices.getTopCoinValue(COIN_URL).enqueue(object : Callback<List<CoinMarketDTO>> {
            override fun onResponse(call: Call<List<CoinMarketDTO>>?, response: Response<List<CoinMarketDTO>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        val body = response.body()!!
                        selfDTOList = body
                        customList.add("---")
                        for (i in 0 until body.size) {
                            val symbol = body[i].symbol
                            customList.add(symbol)
                        }
                        selectedItemFromSpinner()
                    }
                }
            }

            override fun onFailure(call: Call<List<CoinMarketDTO>>?, t: Throwable?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

        })
    }

    private fun selectedItemFromSpinner() {
        val arrayAdapter = ArrayAdapter(applicationContext, android.R.layout.simple_spinner_dropdown_item, customList)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = arrayAdapter
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(applicationContext, "Nothing selected", Toast.LENGTH_LONG).show()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val selectedString = parent?.getItemAtPosition(position).toString()
                coinSymbolSaveButton.isEnabled = false
                if (selectedString != "---") {
                    val symbol = selectedString
                    selectCoinSymbol.text = symbol
                    val name = selfDTOList[position - 1].name
                    selectCoinNameTextView.text = name
                    val price_usd = selfDTOList[position - 1].price_usd
                    selectcoinPriceTextView.text = price_usd
                    val price_pln = selfDTOList[position - 1].price_pln
                    selectCoinPricePLNTextView.text = price_pln

                    coinSymbolSaveButton.isEnabled = true

                    coinSymbolSaveButton.setOnClickListener {
                        val selfRealmDTO = SelfRealmDataDTO(symbol, name, price_usd, price_pln)
                        createCoinRealmRepository(selfRealmDTO)

                        val intent = Intent(applicationContext, SelfActivity::class.java)
                        startActivity(intent)
                    }
                } else {
                    selectCoinSymbol.text = "---"
                    selectCoinNameTextView.text = "---"
                    selectcoinPriceTextView.text = "---"
                    selectCoinPricePLNTextView.text = "---"
                }
            }
        }
    }

    private fun createCoinRealmRepository(selfRealmDTO: SelfRealmDataDTO) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.insert(selfRealmDTO)
        realm.commitTransaction()
        realm.close()
    }

    private fun createSnackbar() {
        val extras: String = intent.extras.get("CreateExtras").toString()
        if (extras == null) {
            snackBarText = "Null extras"
            snackBar = Snackbar.make(selfLayout, snackBarText, Snackbar.LENGTH_LONG)
        } else {
            snackBarText = extras
            snackBar = Snackbar.make(selfLayout, snackBarText, Snackbar.LENGTH_LONG)
            val view = snackBar.view
            val snackbarTextView: TextView = view.findViewById(android.support.design.R.id.snackbar_text)
            snackbarTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        snackBar.show()
    }
}
