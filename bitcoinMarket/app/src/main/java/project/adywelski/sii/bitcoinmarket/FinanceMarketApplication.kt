package project.adywelski.sii.bitcoinmarket

import android.app.Application
import io.realm.Realm
import io.realm.RealmConfiguration
import project.adywelski.sii.bitcoinmarket.components.AppComponent
import project.adywelski.sii.bitcoinmarket.components.DaggerAppStubComponent
import project.adywelski.sii.bitcoinmarket.module.AndroidModule
import project.adywelski.sii.bitcoinmarket.module.NetworkingModule

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class FinanceMarketApplication : Application() {

    private lateinit var component: AppComponent

    fun getAppComponent() : AppComponent {
        return component
    }
    override fun onCreate() {
        super.onCreate()

        component = DaggerAppStubComponent
                .builder()
                .androidModule(AndroidModule(this))
                .networkingModule(NetworkingModule(this))
                .build()

        component.inject(this)

        createRealmDataBase()
    }

    private fun createRealmDataBase(){
        Realm.init(applicationContext)
        val realmConfiguration = RealmConfiguration.Builder()
                .schemaVersion(1)
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(realmConfiguration)
    }
}