package project.adywelski.sii.bitcoinmarket.coinMarket.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.coinMarket.dto.CoinMarketDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class CoinMarketRecyclerViewAdapter(val context: Context, private val coinMarketDTOList: List<CoinMarketDTO>) : RecyclerView.Adapter<CoinMarketViewHolder>() {

    private val layoutInflater : LayoutInflater = LayoutInflater.from(context)

    override fun onBindViewHolder(holder: CoinMarketViewHolder?, position: Int) {
        val coinMarket : CoinMarketDTO = this.coinMarketDTOList[position]
        holder!!.coinRank.text = coinMarket.rank
        holder.coinName.text = coinMarket.name
        holder.coinSymbol.text = coinMarket.symbol
        holder.coinPrice.text = coinMarket.price_usd
        holder.coinPricePLN.text = coinMarket.price_pln
        holder.bind(coinMarket)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): CoinMarketViewHolder {
        val view: View = layoutInflater.inflate(R.layout.coin_market_view_item, parent, false)

        return CoinMarketViewHolder(view)
    }

    override fun getItemCount(): Int {
        return coinMarketDTOList.size
    }
}