package project.adywelski.sii.bitcoinmarket.services

import project.adywelski.sii.bitcoinmarket.coinMarket.dto.CoinMarketDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
interface CoinMarketWebServices {

    @GET
    fun getTopCoinValue(@Url url: String): Call<List<CoinMarketDTO>>
}