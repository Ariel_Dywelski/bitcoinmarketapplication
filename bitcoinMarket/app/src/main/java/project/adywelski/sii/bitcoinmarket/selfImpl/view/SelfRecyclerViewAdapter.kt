package project.adywelski.sii.bitcoinmarket.selfImpl.view

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.selfImpl.dto.SelfRealmDataDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class SelfRecyclerViewAdapter(val context: Context, private val selfDTOList: List<SelfRealmDataDTO>) : RecyclerView.Adapter<SelfViewHolder>() {

    private val layoutInflater : LayoutInflater = LayoutInflater.from(context)

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): SelfViewHolder {
        val view : View = layoutInflater.inflate(R.layout.self_view_item, parent, false)

        return SelfViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelfViewHolder?, position: Int) {
        val selfDTO : SelfRealmDataDTO = selfDTOList[position]

        holder!!.selfSymbol.text = selfDTO.coinSymbolString
        holder.selfName.text = selfDTO.coinNameString
        holder.selfPrice.text = selfDTO.coinPriceString
        holder.selfPricePLN.text = selfDTO.coinPricePLNString

        holder.bind(selfDTO)
    }

    override fun getItemCount(): Int {
        return selfDTOList.size
    }
}