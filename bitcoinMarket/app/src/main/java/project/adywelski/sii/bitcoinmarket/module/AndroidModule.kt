package project.adywelski.sii.bitcoinmarket.module

import dagger.Module
import dagger.Provides
import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
@Module
class AndroidModule (private val application: FinanceMarketApplication) {

    @Provides
    @Singleton
    fun provideApplication() = application
}