package project.adywelski.sii.bitcoinmarket.nationalBank.dto

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
data class CurrencyDTO(val currency: String,
                       val code: String,
                       val bid: String,
                       val ask: String)