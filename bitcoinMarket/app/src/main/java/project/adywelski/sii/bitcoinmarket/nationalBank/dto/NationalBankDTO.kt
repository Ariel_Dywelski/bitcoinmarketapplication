package project.adywelski.sii.bitcoinmarket.nationalBank.dto

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
data class NationalBankDTO(val table : String,
                           val tradingDate : String,
                           val rates: List<CurrencyDTO>)