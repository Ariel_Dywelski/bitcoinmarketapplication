package project.adywelski.sii.bitcoinmarket

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import project.adywelski.sii.bitcoinmarket.coinMarket.CoinMarketActivity
import project.adywelski.sii.bitcoinmarket.nationalBank.NationalBankActivity
import project.adywelski.sii.bitcoinmarket.selfImpl.SelfActivity
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ((application as FinanceMarketApplication)).getAppComponent().inject(this)

        if (permissionChecker()) {
            new_fab.setOnClickListener { view ->
                Snackbar.make(view, "Take a photo :D", Snackbar.LENGTH_LONG).show()

                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (takePictureIntent.resolveActivity(packageManager) != null) {
                    startActivityForResult(takePictureIntent, 123)
                }
            }
        } else {
            permissionChecker()
        }
        coinMarketButton.setOnClickListener {
            val coinMarketIntent = Intent(applicationContext, CoinMarketActivity::class.java)
            coinMarketIntent.putExtra("coinExtras", "Welcome in Coin Market Activity")
            startActivity(coinMarketIntent)
        }

        nationalBankButton.setOnClickListener {
            val nationalBankIntent = Intent(applicationContext, NationalBankActivity::class.java)
            nationalBankIntent.putExtra("extras", "Welcome in National Bank Activity")
            startActivity(nationalBankIntent)
        }

        selfButton.setOnClickListener {
            val selfIntent = Intent(applicationContext, SelfActivity::class.java)
            startActivity(selfIntent)
        }

        menuActivityButton.setOnClickListener {
            val menuIntent = Intent(applicationContext, MenuActivity::class.java)
            startActivity(menuIntent)
        }
    }

    private fun permissionChecker() : Boolean{
        if (ContextCompat.checkSelfPermission(applicationContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 111)
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            val extras : Bundle? = data!!.extras
            val imageBitmap : Bitmap = extras!!.get("data") as Bitmap
            takePhoto.setImageBitmap(imageBitmap)

            onPhotoClickListener(imageBitmap)
        }
        if (requestCode == 456 && resultCode == Activity.RESULT_OK) {
            dispatchTakePictureIntent()
        }
    }

    private fun onPhotoClickListener(imageBitmap: Bitmap) {
        takePhoto.setOnClickListener { createAlertDialog("bla", "blabla", imageBitmap) }
    }

    private fun createAlertDialog(title : String, message : String, imageBitmap: Bitmap) {
        return AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_menu_upload)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("SAVE") { dialog, which ->
                    val startIntent = Intent(applicationContext, SharePhotoActivity::class.java)
                    startIntent.putExtra("Bitmap", imageBitmap)
                    startActivity(startIntent)
                    dialog.dismiss()
                }
                .show()
                .create()
    }
    private var currentPhotoPath: String = ""

    @SuppressLint("SimpleDateFormat")
    private fun createCustomPhotoFile() : File {
        val timestamp : String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val photoFileName : String = "JPEG_"+timestamp+"_"
        val storageDir : File = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val photo : File = File.createTempFile(photoFileName, ".jpg",storageDir)

        currentPhotoPath = photo.absolutePath
        return photo
    }

    private fun dispatchTakePictureIntent(){
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            val photoFile: File = createCustomPhotoFile()

            if (photoFile != null) {
                val photoURI: Uri = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, 456)
            }
        }
    }
}
