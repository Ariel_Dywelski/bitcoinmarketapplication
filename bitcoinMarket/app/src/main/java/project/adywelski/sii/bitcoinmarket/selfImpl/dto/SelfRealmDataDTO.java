package project.adywelski.sii.bitcoinmarket.selfImpl.dto;

import android.os.Parcel;
import android.os.Parcelable;

import io.realm.RealmObject;

/**
 * Created by ariel_dywelski on 18/10/2017.
 */

public class SelfRealmDataDTO extends RealmObject implements Parcelable {

    private String coinSymbolString;
    private String coinNameString;
    private String coinPriceString;
    private String coinPricePLNString;

    public SelfRealmDataDTO(String coinSymbolString, String coinNameString, String coinPriceString, String coinPricePLNString) {
        this.coinSymbolString = coinSymbolString;
        this.coinNameString = coinNameString;
        this.coinPriceString = coinPriceString;
        this.coinPricePLNString = coinPricePLNString;
    }

    public SelfRealmDataDTO(){
    }

    protected SelfRealmDataDTO(Parcel in) {
        coinSymbolString = in.readString();
        coinNameString = in.readString();
        coinPriceString = in.readString();
        coinPricePLNString = in.readString();
    }

    public static final Creator<SelfRealmDataDTO> CREATOR = new Creator<SelfRealmDataDTO>() {
        @Override
        public SelfRealmDataDTO createFromParcel(Parcel in) {
            return new SelfRealmDataDTO(in);
        }

        @Override
        public SelfRealmDataDTO[] newArray(int size) {
            return new SelfRealmDataDTO[size];
        }
    };

    public String getCoinSymbolString() {
        return coinSymbolString;
    }

    public void setCoinSymbolString(String coinSymbolString) {
        this.coinSymbolString = coinSymbolString;
    }

    public String getCoinNameString() {
        return coinNameString;
    }

    public void setCoinNameString(String coinNameString) {
        this.coinNameString = coinNameString;
    }

    public String getCoinPriceString() {
        return coinPriceString;
    }

    public void setCoinPriceString(String coinPriceString) {
        this.coinPriceString = coinPriceString;
    }

    public String getCoinPricePLNString() {
        return coinPricePLNString;
    }

    public void setCoinPricePLNString(String coinPricePLNString) {
        this.coinPricePLNString = coinPricePLNString;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(coinSymbolString);
        dest.writeString(coinNameString);
        dest.writeString(coinPriceString);
        dest.writeString(coinPricePLNString);
    }
}
