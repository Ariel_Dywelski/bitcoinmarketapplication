package project.adywelski.sii.bitcoinmarket.coinMarket

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_coin_market.*
import project.adywelski.sii.bitcoinmarket.FinanceMarketApplication
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.coinMarket.dto.CoinMarketDTO
import project.adywelski.sii.bitcoinmarket.coinMarket.view.CoinMarketRecyclerViewAdapter
import project.adywelski.sii.bitcoinmarket.services.CoinMarketWebServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CoinMarketActivity : AppCompatActivity() {

    @Inject lateinit var webService: CoinMarketWebServices

    private var snackBarText : String = ""
    private lateinit var coinLayout : View
    private lateinit var snackBar : Snackbar
    val COIN_URL : String = "https://api.coinmarketcap.com/v1/ticker/?convert=PLN&limit=20"

    lateinit var recyclerView : RecyclerView
    private lateinit var swipeLayout : SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_coin_market)

        ((application as FinanceMarketApplication)).getAppComponent().inject(this)

        coinLayout = findViewById(R.id.coinMarketLayout)
        recyclerView = findViewById(R.id.coinMarketRecycler)
        swipeLayout = findViewById(R.id.swipeRefreshLayout)

        createSnackbar()
        coinFloatingButton()

        swipeLayout.setOnRefreshListener { getCoinMarketData() }

        getCoinMarketData()
    }

    private fun getCoinMarketData() {
        webService.getTopCoinValue(COIN_URL).enqueue(object : Callback<List<CoinMarketDTO>> {
            override fun onResponse(call: Call<List<CoinMarketDTO>>?, response: Response<List<CoinMarketDTO>>?) {
                if (response != null) {
                    if (response.isSuccessful) {
                        val body: List<CoinMarketDTO> = response.body()!!
                        if (body.isEmpty()) {
                            Toast.makeText(applicationContext, "Failed", Toast.LENGTH_LONG).show()
                            return
                        } else {
                            Toast.makeText(applicationContext, "Success", Toast.LENGTH_LONG).show()
                            val coinMarketRecyclerAdapter = CoinMarketRecyclerViewAdapter(applicationContext, body)
                            recyclerView.adapter = coinMarketRecyclerAdapter
                            recyclerView.layoutManager = LinearLayoutManager(applicationContext)

                            onItemLoadComplete()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<CoinMarketDTO>>?, t: Throwable?) {
                Toast.makeText(applicationContext, "Failed connection", Toast.LENGTH_LONG).show()
                finish()
            }
        })
    }

    private fun onItemLoadComplete(){
        swipeLayout.isRefreshing = false
    }

    private fun coinFloatingButton() {
        coinMarketFloating.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun createSnackbar() {
        val extras = intent.extras.get("coinExtras").toString()

        if (extras == null) {
            snackBarText = "No extras"
            snackBar = Snackbar.make(coinLayout, snackBarText, Snackbar.LENGTH_LONG)
        } else {
            snackBarText = extras
            snackBar = Snackbar.make(coinLayout, snackBarText, Snackbar.LENGTH_LONG)

            val snackBarView = snackBar.view
            val snackBarTextView: TextView = snackBarView.findViewById(android.support.design.R.id.snackbar_text)
            snackBarTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        snackBar.show()
    }
}
