package project.adywelski.sii.bitcoinmarket.module

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import project.adywelski.sii.bitcoinmarket.services.CoinMarketWebServices
import project.adywelski.sii.bitcoinmarket.services.NationalBankWebServices
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
@Module
class NetworkingModule (private val applicationContext: Context) {

    val BASE_URL : String = "https://api.coinmarketcap.com/v1/ticker/?convert=PLN&limit=20"

    @Provides
    @Singleton
    fun provideApplicationContext () = applicationContext

    @Provides
    @Singleton
    fun provideGson() : Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideOkHttpClient() : OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
        builder.addInterceptor(httpLoggingInterceptor)

        return builder.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient) : Retrofit {
        return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
    }

    @Provides
    @Singleton
    fun provideWebService(retrofit: Retrofit) : CoinMarketWebServices {
        return retrofit.create(CoinMarketWebServices::class.java)
    }

    @Provides
    @Singleton
    fun provideNationalBankWebService(retrofit: Retrofit) : NationalBankWebServices {
        return retrofit.create(NationalBankWebServices::class.java)
    }
}