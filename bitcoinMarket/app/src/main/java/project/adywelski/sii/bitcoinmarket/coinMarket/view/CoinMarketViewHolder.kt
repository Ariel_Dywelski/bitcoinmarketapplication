package project.adywelski.sii.bitcoinmarket.coinMarket.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.coinMarket.dto.CoinMarketDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class CoinMarketViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

    var coinRank : TextView = itemView.findViewById(R.id.coinRankTextView)
    var coinName : TextView = itemView.findViewById(R.id.coinNameTextView)
    var coinSymbol : TextView = itemView.findViewById(R.id.coinSymbolTextView)
    var coinPrice : TextView = itemView.findViewById(R.id.coinPriceTextView)
    var coinPricePLN : TextView = itemView.findViewById(R.id.coinPricePLNTextView)

    fun bind(coinMarket: CoinMarketDTO){
        coinRank.text = coinMarket.rank
        coinName.text = coinMarket.name
        coinSymbol.text = coinMarket.symbol
        coinPrice.text = coinMarket.price_usd
        coinPricePLN.text = coinMarket.price_pln
    }
}