package project.adywelski.sii.bitcoinmarket.selfImpl.view

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import project.adywelski.sii.bitcoinmarket.R
import project.adywelski.sii.bitcoinmarket.selfImpl.dto.SelfRealmDataDTO

/**
 * Created by ariel_dywelski on 17/10/2017.
 */
class SelfViewHolder (itemView : View) : RecyclerView.ViewHolder(itemView) {
    var selfSymbol : TextView = itemView.findViewById(R.id.selfSymbol)
    var selfName : TextView = itemView.findViewById(R.id.selfName)
    var selfPrice : TextView = itemView.findViewById(R.id.selfPrice)
    var selfPricePLN : TextView = itemView.findViewById(R.id.selfPricePLN)

    fun bind(selfDTO: SelfRealmDataDTO){
        selfSymbol.text = selfDTO.coinSymbolString
        selfName.text = selfDTO.coinNameString
        selfPrice.text = selfDTO.coinPriceString
        selfPricePLN.text = selfDTO.coinPricePLNString
    }
}